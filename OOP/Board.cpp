#include "Board.h"

void Board::make_empy_board() {

	for (int i = 0; i < Columns; i++) {
		board[i] = (Soldier **)malloc(sizeof(Soldier *) * Rows);
		for (int j = 0; j < Rows; j++) {
			board[i][j] = (Soldier *)malloc(sizeof(Soldier));
			board[i][j]->set_position(i, j);
			board[i][j]->set_status(inactive);
		}
	}
}

void Board::insert_soldier_to_board(Soldier sol) {

	Point p = sol.get_position();
	int x = p.get_x();
	int y = p.get_y();
	board[x][y] = &sol;
}

void Board::reset_cell(int x, int y) {

	board[x][y] = NULL;
}

Soldier * Board::get_soldier_in_cell(int x, int y) {
	return board[x][y];
}

bool Board::is_place_available(int x, int y) {

	if (board[x][y]->get_status() == active) {
		return false;
	}
	return true;
}

//Player Board::place_belongs_to(int x, int y) {
//	//return board[x][y]->get_belongs_to();
//}
