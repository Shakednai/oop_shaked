#include "Game.h"
#include "Config.h"



void Game::start_game() {
	board.make_empy_board();
}

void Game::set_players_soldier(Player &p) {

	for (int i = 0; i < max_soldiers; i++) {
		//get line from position file - get type, x, y and insert to the function I put an example here
		p.soldiers[i].set_soldier(R, 3, 4);
		p.soldiers[i].set_status(active);
		board.insert_soldier_to_board(p.soldiers[i]);
	}
}

void Game::play(Player &p) {
	//get line from moves file
	//get x, y from and x,y to move
	//I put an example here
	//while there is line and no EOF
	while (true) {
		for (int i = 0; i < max_soldiers; i++) {
			Point currentPos = p.soldiers[i].get_position();
			if (p.soldiers[i].get_status() == active && currentPos.get_x() == 3 && currentPos.get_y() == 5) {
				p.soldiers[i].set_position(3, 4);
				
			}
		}
	}
 }

Win Game::check_winner() {

	int points1 = p1.get_points();
	int points2 = p2.get_points();
	if (points1 == points2) {
		if (points1 == 0) {
			return TIE;
		}
		if (points1 > 0) {
			return Both;
		}
	}
	if (points1 > points2) {
		return P1;
	}
	else {
		return P2;
	}
}

void Game::fight(Soldier sol1, Soldier sol2) {

	bool is_set = false;
	TYPES type_1 = sol1.get_type();
	TYPES type_2 = sol2.get_type();
	TYPES possible_winner[4] = { F,F,F,F };

	switch (type_1) {
	case(P):
		possible_winner[0] = R;
		break;
	case(R):
		possible_winner[0] = S;
		break;
	case(S):
		possible_winner[0] = P;
		break;
	case(B):
		possible_winner[0] = R;
		possible_winner[1] = S;
		possible_winner[2] = P;
		break;
	}

	for (int i = 0; i < 4; i++) {
		if (type_2 == possible_winner[i]) {
			sol2.set_status(inactive);
			is_set = true;
		}
	}

	if (!is_set) {
		sol1.set_status(inactive);
	}
}

void Game::conflict(Soldier s1, Soldier s2, Board board) {

	if (s1.get_belongs_to() == s2.get_belongs_to()) {
		//add +point to the oppenent and stop the game
	}
	else {
		fight(s1, s2);
	}

}