#pragma once
#include "soldier.h"
#include "Config.h"
#include <stdlib.h>
#include "player.h"

class Board{

	Soldier ***board = (Soldier ***)malloc(Columns * sizeof(Soldier **));

public:

	void make_empy_board();
	void insert_soldier_to_board(Soldier sol);
	bool is_place_available(int x, int y);
	//Player place_belongs_to(int x, int y);
	void reset_cell(int x, int y);
	Soldier *get_soldier_in_cell(int x,int y);



};