#pragma once

#include <stdio.h>	
#include <stdlib.h>
#include "soldier.h"
#include "Config.h"


class Player {

	int points = 0;
	int index = 0;

public:

	Soldier soldiers[max_soldiers];


	bool insert_soldier_to_player(Soldier sol);
	int get_num_of_active_moveable_soldiers();
	Soldier* get_soldiers() { return soldiers; }
	void set_point() { points++; }
	int get_points() { return points; }
	

};