#include "Point.h"

bool Point::is_point_in_range(int max_x, int max_y) {
	if (x >= 0 && x <= max_x && y >= 0 && y <= max_y) {
		return true;
	}
	return false;
}