#pragma once

using namespace std;
#include <map>;

enum TYPES { R, P, S, B, F };
enum STATUS { inactive, active };
enum Win {P1, P2, Both, TIE};
enum Flat_Players{PlyerA, PlayerB};

int const Rows = 10;
int const Columns = 10;
int const num_of_R = 2;
int const num_of_P = 5;
int const num_of_S = 1;
int const num_of_B = 2;
int const num_of_F = 1;
int const num_of_J = 2;
int const max_soldiers = 13;

