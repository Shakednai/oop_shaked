#pragma once
#include "soldier.h"
#include "player.h"
#include "Board.h"
#include "Config.h"


class Game {

	Player p1, p2;
	Board board;
	int visible_mode = 0;
	Win status;
	void set_players_soldier(Player &p);

public:

	void start_game();
	Win check_winner();
	void fight(Soldier sol1, Soldier sol2);
	void conflict(Soldier s1, Soldier s2, Board board);
	void play(Player &p);
	bool step_validator();

};